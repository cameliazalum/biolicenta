
import sys

from PyQt5.QtWidgets import QApplication
from PyQt5.QtWidgets import QLabel
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QHBoxLayout
from PyQt5.QtWidgets import QPushButton
from PyQt5.QtWidgets import QWidget
from PyQt5.QtWidgets import QLineEdit
import aliniamentGlobal
import aliniamentLocal
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMainWindow, QPushButton, QApplication, QLabel

data = []
# definitia modelului abstract pentru tabele
class TableModel(QtCore.QAbstractTableModel):
    def __init__(self, parent, tabledata):
        QtCore.QAbstractTableModel.__init__(self, parent)
        # setarea datelor pentru tabele si culorilor
        self.modelTableData = tabledata
        self.background_colors = dict()

    def rowCount(self, parent=QtCore.QModelIndex()):
        return len(self.modelTableData)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return len(self.modelTableData[0])

    def data(self, index, role):
        if not index.isValid():
            return None
        if (
            0 <= index.row() < self.rowCount()
            and 0 <= index.column() < self.columnCount()
        ):
            if role == QtCore.Qt.BackgroundRole:
                ix = self.index(index.row(), index.column())
                pix = QtCore.QPersistentModelIndex(ix)
                if pix in self.background_colors:
                    color = self.background_colors[pix]
                    return color ## adaugarea culorii pe celula cu indecsii corespunzatori
            elif role == QtCore.Qt.DisplayRole:
                return self.modelTableData[index.row()][index.column()] ## si adaugarea valorilor fara culori

    ## setarea tuturor celulelor tabelelor cu valoare si culoare 
    def setData(self, index, value, role):
        if not index.isValid():
            return False
        if (
            0 <= index.row() < self.rowCount()
            and 0 <= index.column() < self.columnCount()
        ):
            if role == QtCore.Qt.BackgroundRole and index.isValid():
                ix = self.index(index.row(), index.column())
                pix = QtCore.QPersistentModelIndex(ix)
                self.background_colors[pix] = value
                return True
        return False

  

## clasa pentru fereastra principala cu cele 2 optiuni (aliniament local sau global)
class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.setFixedSize(800, 600)
        btn = QPushButton('Global Alignment', self) ## definirea si amplasarea butonului
        btn.move(50, 200)
        btn.setFixedSize(300, 100)
        btn.clicked.connect(self.openSecond) ## conectarea butoanelor la eventuri de click si functii

        btn2 = QPushButton('Local Alignment', self) ## definirea si amplasarea butonului
        btn2.move(410, 200)
        btn2.setFixedSize(300, 100)
        btn2.clicked.connect(self.openThird) ## conectarea butoanelor la eventuri de click si functii


    def openSecond(self):
        self.SW = SecondWindow() # deschide fereastra a doua pentru alinierea globala
        self.SW.show()

    def openThird(self):
        self.TW = ThirdWindow() # deschide fereastra a treia pentru alinierea locala
        self.TW.show()

class SecondWindow(QMainWindow):
    def __init__(self):
        super(SecondWindow, self).__init__()
        ## adaugarea si setarea tuturor butoanelor, labelurilor si editeline-urilor
        self.setFixedSize(1900, 600)
        self.s1 = QLabel(self)
        self.s1.setText('Secventa 1:')
        self.line1 = QLineEdit(self)

        self.line1.move(120, 20)
        self.line1.resize(200, 32)
        self.s1.move(20, 20)

        self.s2 = QLabel(self)
        self.s2.setText('Secventa 2:')
        self.line2 = QLineEdit(self)

        self.line2.move(120, 60)
        self.line2.resize(200, 32)
        self.s2.move(20, 60)

        self.potrivire = QLabel(self)
        self.potrivire.setText('S. Potrivire:')
        self.potrivireLine = QLineEdit(self)

        self.potrivireLine.move(120, 120)
        self.potrivireLine.resize(200, 32)
        self.potrivire.move(20, 120)

        self.nepotrivire = QLabel(self)
        self.nepotrivire.setText('S. Nepotrivire:')
        self.nepotrivireLine = QLineEdit(self)

        self.nepotrivireLine.move(120, 160)
        self.nepotrivireLine.resize(200, 32)
        self.nepotrivire.move(20, 160)

        self.gap = QLabel(self)
        self.gap.setText('Gap:')
        self.gapLine = QLineEdit(self)

        self.gapLine.move(120, 200)
        self.gapLine.resize(200, 32)
        self.gap.move(20, 200)
        
        genereazaM = QPushButton('Genereaza matricea', self)
        genereazaM.move(350, 40)
        genereazaM.setFixedSize(200, 40)
        ## cenexiune de tip lambda pentru a transmite parametri cate functia din trigger ( trigger = on_click )
        genereazaM.clicked.connect(lambda: self.globalMatrix(self.line1.text(), self.line2.text(), self.potrivireLine.text(), self.nepotrivireLine.text(), self.gapLine.text()))

        backTrace = QPushButton('Back Trace', self)
        backTrace.move(350, 100)
        backTrace.setFixedSize(200, 40)
        backTrace.clicked.connect(self.colorStuff)

        ag = QPushButton('Aliniament Global', self)
        ag.move(650, 20)
        ag.setFixedSize(200, 40)
         ## cenexiune de tip lambda pentru a transmite parametri cate functia din trigger ( trigger = on_click )
        ag.clicked.connect(lambda: self.scoateRezultat(self.line1.text(), self.line2.text(), self.potrivireLine.text(), self.nepotrivireLine.text(), self.gapLine.text()))
        self.agLine1 = QLineEdit(self)
        self.agLine1.move(650, 70)
        self.agLine1.resize(200, 32)
        self.agLine2 = QLineEdit(self)
        self.agLine2.move(650, 110)
        self.agLine2.resize(200, 32)
        self.agLine1.setPlaceholderText("Secventa 1")
        self.agLine2.setPlaceholderText("Secventa 2")

        aliniazaScor = QPushButton('Afisare Scor', self)
        aliniazaScor.move(950, 20)
        aliniazaScor.setFixedSize(200, 40)
        aliniazaScor.clicked.connect(self.setScoreText)
        self.scor = QLineEdit(self)
        self.scor.setPlaceholderText("Scor")
        self.scor.move(950, 110)
        self.scor.resize(200, 32)

    # afiseaza scroul optim
    def setScoreText(self) : 
        self.scor.setText(str(data[-1][-1]))

    # afiseaza tabelul din cadrul functiei de backtrace
    def colorStuff(self):
        self.backTraceTable.show()

    # creaza o bordura la nord, est si vest fata de matricea pricipala pentrua calcula maximele si in cazul numerelor negative
    def setBorder(self, A):
        AA = []
        AA.append([])
        for i in range(len(A[0]) + 2):
            AA[0].append(-1000)
        for i in range(len(A)):
            AA.append([])
            AA[i+1].append(-1000)
            for j in range(len(A[i])) :
                AA[i+1].append(A[i][j])
            AA[i+1]. append(-1000)
        return AA 


    def globalMatrix(self, s1, s2, potrivire, nepotrivire, gap): 
        ## apeloasa algoritmul pentru alinierea globala si trimite ca paramatri secventele si scorurile
        (self.x, self.y, self.parents) = aliniamentGlobal.global_align(s1,s2,int(gap), int(nepotrivire),int( potrivire))
        print(self.x, self.y)
        
        global data
        data = []
        A= aliniamentGlobal.A
        ## determina matricea de costuri
        data.append(list(" "))
        data[0].append(" ")
        for i in s1:
            data[0].append(i)
        for i in range(len(A)):
            if i == 0:
                l = [" "]
                for j in A[0]:
                    l.append(j)
                data.append(l)
            else :
                l = [s2[i-1]]
                for j in A[i]:
                    l.append(j)
                data.append(l)
       
        ## determina matricea pentru backtrace cu colori 
        self.backTraceTable = QtWidgets.QTableView() # creaza tabelul
        self.backTraceModel = TableModel(None, data) # creaza modelul
        i = len(s2) + 1
        j = len(s1) + 1
        self.backTraceModel.setData(self.backTraceModel.index(i,j), QtGui.QColor(QtCore.Qt.green), QtCore.Qt.BackgroundRole)
        for i in self.parents:
            self.backTraceModel.setData(self.backTraceModel.index(i[0] +1,i[1] + 1), QtGui.QColor(QtCore.Qt.green), QtCore.Qt.BackgroundRole)

        self.backTraceTable.setModel(self.backTraceModel) #seteaza modelul de tabel
        ## creeaza, seteaza modelul si afiseaza tabeelul de costuri
        self.table = QtWidgets.QTableView()
        self.model = TableModel(None, data)
        self.table.setModel(self.model)
        self.table.show()

    # afiseaza secventele obtinute 
    def scoateRezultat(self, s1, s2, potrivire, nepotrivire, gap):
        print(self.x, self.y)
        self.agLine1.setText(self.x)
        self.agLine2.setText(self.y)


# la fel ca la second, doar ca pe aliniament local
class ThirdWindow(QMainWindow):
    def __init__(self):
        super(ThirdWindow, self).__init__()
        # lbl = QLabel('Local Alignment', self)
        self.setFixedSize(1900, 600)
        self.s1 = QLabel(self)
        self.s1.setText('Secventa 1:')
        self.line1 = QLineEdit(self)

        self.line1.move(120, 20)
        self.line1.resize(200, 32)
        self.s1.move(20, 20)

        self.s2 = QLabel(self)
        self.s2.setText('Secventa 2:')
        self.line2 = QLineEdit(self)

        self.line2.move(120, 60)
        self.line2.resize(200, 32)
        self.s2.move(20, 60)

        self.potrivire = QLabel(self)
        self.potrivire.setText('S. Potrivire:')
        self.potrivireLine = QLineEdit(self)

        self.potrivireLine.move(120, 120)
        self.potrivireLine.resize(200, 32)
        self.potrivire.move(20, 120)

        self.nepotrivire = QLabel(self)
        self.nepotrivire.setText('S. Nepotrivire:')
        self.nepotrivireLine = QLineEdit(self)

        self.nepotrivireLine.move(120, 160)
        self.nepotrivireLine.resize(200, 32)
        self.nepotrivire.move(20, 160)

        self.gap = QLabel(self)
        self.gap.setText('Gap:')
        self.gapLine = QLineEdit(self)

        self.gapLine.move(120, 200)
        self.gapLine.resize(200, 32)
        self.gap.move(20, 200)
        self.agLine1 = QLineEdit(self)
        self.agLine1.move(650, 70)
        self.agLine1.resize(200, 32)
        self.agLine2 = QLineEdit(self)
        self.agLine2.move(650, 110)
        self.agLine2.resize(200, 32)
        self.agLine1.setPlaceholderText("Secventa 1")
        self.agLine2.setPlaceholderText("Secventa 2")

        genereazaM = QPushButton('Genereaza matricea', self)
        genereazaM.move(350, 40)
        genereazaM.setFixedSize(200, 40)
        genereazaM.clicked.connect(lambda: self.localMatrix(self.line1.text(), self.line2.text(), self.potrivireLine.text(), self.nepotrivireLine.text(), self.gapLine.text()))

        backTrace = QPushButton('Back Trace', self)
        backTrace.move(350, 100)
        backTrace.setFixedSize(200, 40)
        backTrace.clicked.connect(self.colorStuff)
        ag = QPushButton('Aliniament Local', self)
        ag.move(650, 20)
        ag.setFixedSize(200, 40)
        ag.clicked.connect(lambda: self.scoateRezultat(self.line1.text(), self.line2.text(), self.potrivireLine.text(), self.nepotrivireLine.text(), self.gapLine.text()))

        aliniazaScor = QPushButton('Afisare Scor', self)
        aliniazaScor.move(950, 20)
        aliniazaScor.setFixedSize(200, 40)
        self.scor = QLineEdit(self)
        self.scor.setPlaceholderText("Scor")
        self.scor.move(950, 110)
        self.scor.resize(200, 32)
        aliniazaScor.clicked.connect(self.setScoreText)
    
    def scoateRezultat(self, s1, s2, potrivire, nepotrivire, gap):
        print(self.x, self.y)
        self.agLine1.setText(self.x)
        self.agLine2.setText(self.y)

    def setScoreText(self) : 
        self.scor.setText(str(self.best))

    def colorStuff(self):
        self.backTraceTable.show()


    def localMatrix(self, s1, s2, potrivire, nepotrivire, gap): 
        (self.x, self.y, self.parents, self.place, self.best) = aliniamentLocal.local_align(s1,s2,int(gap), int(nepotrivire),int( potrivire))
        print(self.best, self.place[0], self.place[1])
        A= aliniamentLocal.A
        global data
        data = []
        data.append(list(" "))
        data[0].append(" ")
        for i in s1:
            data[0].append(i)
        for i in range(len(A)):
            if i == 0:
                l = [" "]
                for j in A[0]:
                    l.append(j)
                data.append(l)
            else :
                l = [s2[i-1]]
                for j in A[i]:
                    l.append(j)
                data.append(l)
       
        self.backTraceTable = QtWidgets.QTableView()
        self.backTraceModel = TableModel(None, data)
        i = len(s2) + 1
        j = len(s1) + 1
        
        self.backTraceModel.setData(self.backTraceModel.index(self.place[0]+1,self.place[1]+1), QtGui.QColor(QtCore.Qt.green), QtCore.Qt.BackgroundRole)
        
        for i in self.parents:
            self.backTraceModel.setData(self.backTraceModel.index(i[0] +1,i[1] + 1), QtGui.QColor(QtCore.Qt.green), QtCore.Qt.BackgroundRole)

        self.backTraceTable.setModel(self.backTraceModel)

        self.table = QtWidgets.QTableView()
        self.model = TableModel(None, data)
        self.table.setModel(self.model)
        self.table.show()




# de aici porneste aplicatia
if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    MW = MainWindow()
    MW.show()
    sys.exit(app.exec_())