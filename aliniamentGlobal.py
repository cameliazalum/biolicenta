

# clasa pentru parametrii de scor
class ScoreParam:
    
    def __init__(self, gap, mismatch, match):
        self.gap = gap
        self.mismatch = mismatch
        self.match = match
    def match(self, chr):
        return self.match
A = []
X = ""
Y = ""
parents = []
def global_align(x, y, a, b, c):
    global A
    A = []
    
    global parents
    parents = []
    score = ScoreParam(a, b, c)
    #creaza matricea de costuri/ scor
    for i in range(len(y) + 1):
        A.append([0] * (len(x) +1))
    for i in range(len(y)+1):
        A[i][0] = score.gap * i
    for i in range(len(x)+1):
        A[0][i] = score.gap * i
    for i in range(1, len(y) + 1):
        for j in range(1, len(x) + 1):
            A[i][j] = max(
            A[i][j-1] + score.gap,
            A[i-1][j] + score.gap,
            A[i-1][j-1] + (score.match if y[i-1] == x[j-1] else score.mismatch)
            )
    align_X = ""
    align_Y = ""
    i = len(x)
    j = len(y)
    # creaza secventele in aliniament global
    while i > 0 or j > 0:
         
        current_score = A[j][i]

        if i > 0 and j > 0 and x[i - 1] == y[j - 1]:
            align_X = x[i - 1] + align_X
            align_Y = y[j - 1] + align_Y
            parents.append((j-1, i-1))
            i = i - 1
            j = j - 1
         
        elif i > 0 and (current_score == A[j][i - 1] + score.mismatch or current_score == A[j][i - 1] + score.gap):
            align_X = x[i - 1] + align_X
            align_Y = "-" + align_Y
            parents.append((j, i-1))
            i = i - 1
             
        else:
            align_X = "-" + align_X
            align_Y = y[j - 1] + align_Y
            parents.append((j-1, i))
            j = j - 1
    
    X = align_X
    Y = align_Y 
    # print(A)
    # print("parents are")
    # print(parents)
    return (align_X, align_Y, parents)

# secventa de test locala
# xx="AGTA"
# yy="CTGAA"
# global_align(xx, yy, -2, -1, 2)

