class ScoreParam:

    def __init__(self, gap, mismatch, match):
        self.gap = gap
        self.mismatch = mismatch
        self.match = match
  
A = []
parents= []
def local_align(x, y, a, b, c):
    score=ScoreParam(a, b, c)
   
    global A
    global parents
    parents = []
    A = []
    for i in range(len(y) + 1):
        A.append([0] * (len(x) +1))
    best = 0
    optloc = (0,0)

    for i in range(1, len(y) + 1):
        for j in range(1, len(x) + 1):
           
            A[i][j] = max(
            A[i][j-1] + score.gap,
            A[i-1][j] + score.gap,
            A[i-1][j-1] + (score.match if y[i-1] == x[j-1] else score.mismatch),
            0
            )
           
            if A[i][j] >= best:
                best = A[i][j]
                optloc = (i,j)

    align_X = ""
    align_Y = ""
    j, i = optloc
    while (i > 0 or j > 0) and A[j][i] > 0:
         
        current_score = A[j][i]

        if i > 0 and j > 0 and x[i - 1] == y[j - 1]:
            align_X = x[i - 1] + align_X
            align_Y = y[j - 1] + align_Y
            parents.append((j-1, i-1))
            i = i - 1
            j = j - 1
         
        elif i > 0 and (current_score == A[j][i - 1] + score.mismatch or current_score == A[j][i - 1] + score.gap) and A[j][i - 1] > 0:
            align_X = x[i - 1] + align_X
            align_Y = "-" + align_Y
            parents.append((j, i-1))
            i = i - 1
             
        else:
            align_X = "-" + align_X
            align_Y = y[j - 1] + align_Y
            parents.append((j-1, i))
            j = j - 1
 
    # print(align_X)
    # print(align_Y)
    print(A)
    return (align_X, align_Y, parents, optloc, best)

# xx = 'ACA'
# yy='CTA'
# local_align(xx, yy, -1 ,1, 2)